import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CrudIndex from './src/crud/index.js';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <CrudIndex />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    flex: 1,
  },
});

export default App;
