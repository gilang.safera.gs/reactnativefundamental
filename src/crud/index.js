import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';

class CrudIndex extends Component {
  state = {
    id: '',
    name: '',
    address: '',
    isUpdate: false,
    data: [
      {id: 1, name: 'Gilang', address: 'Tangerang'},
      {id: 2, name: 'Ari', address: 'Tangerang'},
      {id: 3, name: 'Dany', address: 'Tangerang'},
    ],
  };

  handleUserInputText = (value) => {
    this.setState({
      name: value,
    });
    console.log(this.state.name);
  };

  handleUserInputAddres = (value) => {
    this.setState({
      address: value,
    });
  };

  handleAddUserData = () => {
    let newUser = {
      id: this.state.data.length + 1,
      name: this.state.name,
      address: this.state.address,
    };
    this.setState({
      data: [...this.state.data, newUser],
    });
  };
  handleDeleteUser = (index) => {
    console.log(index);
    let newData = this.state.data;
    newData.splice(index, 1);
    this.setState({
      data: newData,
    });
    console.log(newData);
  };
  handleEditUser = (index) => {
    const data = this.state.data[index];
    console.log(data);
    this.setState({
      id: index,
      name: data.name,
      address: data.address,
      isUpdate: true,
    });
  };
  handleUpdateData = () => {
    let data = this.state.data;
    const index = this.state.id;
    data[index] = {
      id: index + 1,
      name: this.state.name,
      address: this.state.address,
    };
    this.setState({
      id: '',
      name: '',
      address: '',
      isUpdate: false,
      data: data,
    });
  };

  showToast(msg) {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  }
  render() {
    return (
      <View>
        <Text style={styles.titleSection}>Simple Crud Section</Text>
        <View>
          <View style={styles.formSection}>
            <TextInput
              placeholder="Insert Name"
              value={this.state.name}
              onChangeText={(input) => this.handleUserInputText(input)}
            />
          </View>
          <View style={styles.formSection}>
            <TextInput
              placeholder="Insert Address"
              value={this.state.address}
              onChangeText={(input) => this.handleUserInputAddres(input)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
              marginBottom: 12,
              paddingHorizontal: 12,
            }}>
            <TouchableOpacity onPress={this.handleAddUserData}>
              <View style={styles.buton}>
                <Text style={{color: 'white'}}>Button Add</Text>
              </View>
            </TouchableOpacity>
            {this.state.isUpdate ? (
              <TouchableOpacity
                onPress={() => {
                  this.handleUpdateData();
                }}>
                <View style={styles.butonUpdate}>
                  <Text style={{color: 'white'}}>Update Data</Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
          <Text> List Data</Text>
          <View style={styles.lineSection}></View>
        </View>
        {/* Item List Data */}
        {this.state.data.map((items, index) => {
          return (
            <View key={index} style={styles.itemRecycler}>
              <View style={{flexDirection: 'row'}}>
                <Text>{index + 1}. </Text>
                <Text>{items.name} | </Text>
                <Text>{items.address}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                {/* Button Delete */}
                <TouchableOpacity
                  onPress={() => {
                    this.handleDeleteUser(index);
                  }}>
                  <Text style={{color: 'red', fontWeight: 'bold'}}>Delete</Text>
                </TouchableOpacity>
                <Text> | </Text>
                {/* Button Edit */}
                <TouchableOpacity
                  onPress={() => {
                    this.handleEditUser(index);
                  }}>
                  <Text style={{color: 'orange', fontWeight: 'bold'}}>
                    Edit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  formSection: {
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 12,
    marginTop: 8,
  },
  buton: {
    width: 150,
    height: 35,
    backgroundColor: 'salmon',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 12,
    borderRadius: 6,
    alignSelf: 'flex-end',
  },
  butonUpdate: {
    width: 150,
    height: 35,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 12,
    borderRadius: 6,
    alignSelf: 'flex-end',
  },
  titleSection: {
    fontWeight: 'bold',
    alignSelf: 'center',
    marginBottom: 24,
    fontSize: 21,
  },
  lineSection: {
    height: 5,
    backgroundColor: 'salmon',
    borderRadius: 8,
    marginTop: 12,
  },
  itemRecycler: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 14,
  },
});

export default CrudIndex;
