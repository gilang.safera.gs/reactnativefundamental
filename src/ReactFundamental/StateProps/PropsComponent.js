import React, {Component} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

class PropsComponents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Props and Component',
      parent_component: this.props.parent_name,
    };

    PropsComponents.defaultProps = {
      parent_name: 'Default Props',
    };
  }

  handleUpdateParent = (value) => {
    this.props.update_parent(value);
  };

  render() {
    return (
      <View>
        <Text style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>
          Child Component
        </Text>
        <Text>Component name : {this.state.name}</Text>
        <Text>Parent Name : {this.props.parent_name}</Text>
        <Button
          title="update parent"
          onPress={() => {
            this.handleUpdateParent('Parent Update from Child');
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
});

export default PropsComponents;
